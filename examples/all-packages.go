package main

import (
	"fmt"
	"time"

	env "gitlab.com/hoplite/golang-tools/pkg/env"
	retry "gitlab.com/hoplite/golang-tools/pkg/retry"
	"gitlab.com/hoplite/golang-tools/pkg/uuid_tools"
)

const (
	DefaultApiRetries = 3
)

func main() {
	retries := env.GetEnvInt("MY_API_RETRIES", DefaultApiRetries)
	err := retry.Retry(retries, time.Second, func() error {
		//Try to validate a known bad UUID.
		err := uuid_tools.ValidateUuid(
			"aa29896-be88-403e-a906-7442bef33b87",
			4,
		)
		return err
	})
	if err != nil {
		fmt.Printf(
			"A terrible thing happened %s\n",
			err.Error(),
		)
	}
}
