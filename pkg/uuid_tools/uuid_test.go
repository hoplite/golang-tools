package uuid_tools

import (
	"testing"
)

func TestValidateUUID(t *testing.T) {
	err := ValidateUuid(
		"1aa29896-be88-403e-a906-7442bef33b87",
		4,
	)
	if err != nil {
		t.Fatalf("Got err %v. Want err %v", err, nil)
	}
}

func TestValidateInvalidUUID(t *testing.T) {
	err := ValidateUuid(
		"aa29896-be88-403e-a906-7442bef33b87",
		4,
	)
	if err != nil && err.Error() != InvalidUuidErr.Error() {
		t.Fatalf("Got err %s, want err %s", err, InvalidUuidErr)
	}
}

func TestValidateInvalidUUIDVersion(t *testing.T) {
	err := ValidateUuid(
		"1aa29896-be88-403e-a906-7442bef33b87",
		2,
	)
	if err != nil && err.Error() != InvalidUuidVersionErr.Error() {
		t.Fatalf("Got err %s, want err %s", err, InvalidUuidVersionErr)
	}
}
