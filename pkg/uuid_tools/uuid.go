package uuid_tools

import (
	"errors"

	"github.com/gofrs/uuid"
)

var (
	InvalidUuidErr        = errors.New("Id is not a valid UUID!")
	InvalidUuidVersionErr = errors.New("UUID is not the correct version!")
)

func ValidateUuid(idString string, version int) error {
	id, err := uuid.FromString(idString)
	if err != nil {
		return InvalidUuidErr
	}
	if int(id.Version()) != version {
		return InvalidUuidVersionErr
	}
	return nil
}
