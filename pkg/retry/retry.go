package retry

import (
	"log"
	"math/rand"
	"time"
)

// Perform recursive retries until don't have anymore
// Inspired by this blog https://upgear.io/blog/simple-golang-retry-function/
func Retry(attempts int, sleep time.Duration, f func() error) error {
	if err := f(); err != nil {
		if attempts--; attempts > 0 {
			log.Printf("Retrying attempt %v", attempts)
			// Add some randomness to prevent creating a Thundering Herd
			jitter := time.Duration(rand.Int63n(int64(sleep)))
			sleep = sleep + jitter/2

			time.Sleep(sleep)
			return Retry(attempts, 2*sleep, f)
		}
		log.Printf("Failed after max retries!")
		return err
	}

	return nil
}
