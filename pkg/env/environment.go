package env

import (
	"os"
	"strconv"
)

func getEnv(envVar string, defaultValue string) string {
	if value, exists := os.LookupEnv(envVar); exists {
		return value
	}
	return defaultValue
}

// Golang env values are string, if they're supposed to be int, we need to convert them.
func GetEnvInt(envVar string, defaultValue int) int {
	valueStr := getEnv(envVar, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}
	return defaultValue
}

func GetEnvBool(envVar string, defaultValue bool) bool {
	valueStr := getEnv(envVar, "")
	if value, err := strconv.ParseBool(valueStr); err == nil {
		return value
	}

	return defaultValue
}
